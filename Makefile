CC = g++
CFLAGS = -g -Wall
ROOT_DIR = .
OBJ_DIR = $(ROOT_DIR)/obj
OUT_DIR = $(ROOT_DIR)/bin

all: preparation test_pointer main
	@echo Successfully compiled

preparation:
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(OUT_DIR)

smart_pointer:
	$(CC) $(CFLAGS) -c $(ROOT_DIR)/src/smart_pointer.cpp -o $(OBJ_DIR)/smart_pointer.o
	@echo smart_pointer compiled

test_pointer:
	$(CC) $(CFLAGS) -c $(ROOT_DIR)/src/counted_ptr.cpp -o $(OBJ_DIR)/counter_ptr.o
	@echo smart_pointer compiled

main:
	$(CC) $(CFLAGS) $(ROOT_DIR)/src/main.cpp -o $(OUT_DIR)/main
	@echo main compiled

clean:
	rm -rf $(OBJ_DIR)/*.o
