# README #

Basic wrapper around a given raw C pointer. The wrapper is "unique" in the way that it cannot be copied among instances of the wrapper object.