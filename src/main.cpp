#include <iostream>

#include "unique_ptr.h"

class Person;
void run_example_usage();

/**
Class just for testing purposes
*/
class Person
{
	public:

		void speak()const
		{
		}

		std::string m_name;
};

/**
Just run sample usage example
*/
void run_example_usage()
{
	burda::unique_ptr<Person> ptr_n1(new Person());
	const burda::unique_ptr<Person> ptr_n2(NULL);

	Person * raw1 = ptr_n1.get();
    (*ptr_n1)->m_name;
    ptr_n2->m_name;
	ptr_n2->speak();

	// following operations are disabled:
	// n = n3;
	// burda::unique_ptr<Person> n4(n);
	// burda::unique_ptr<Person> n2 = n;
}

int main(void)
{
    run_example_usage();

	return 0;
}
