#ifndef UNIQUE_PTR_INCLUDED
#define UNIQUE_PTR_INCLUDED

#include <iostream>

namespace burda
{

	/**
	Sample wrapper around raw pointer of parametrized class.
	Karel Burda, 2009.
	*/

	template<typename T>
	class unique_ptr
	{

        public:

			/**
			Constructor taking the raw pointer
			@param Pointer
			*/
    		explicit unique_ptr(T * p_raw_ptr = 0) throw()
    		{
    		    this->m_raw_ptr = 0;
                this->m_raw_ptr = p_raw_ptr;
    		}

			/**
			Destructor
			*/
    		~unique_ptr() throw()
    		{
                this->release();
    		}

			/**
			Operator dereference
			Usage:
                class Person;
				burda::unique_ptr<Person> my_ptr(new Person());
				(*my_ptr)->m_name = "new name";
			*/
    		T* operator*() const throw()
			{
				return this->m_raw_ptr;
			}

			/**
			Operator arrow - member access
			Usage:
                class Person;
				burda::unique_ptr<Person> my_ptr(new Person());
				my_ptr->m_name = "new name";
			*/
            T* operator->() const throw()
			{
				return this->m_raw_ptr;
			}

			/**
			Sample getter
			@returns inner raw pointer
			*/
            T * get() const throw()
            {
                return this->m_raw_ptr;
            }

			/**
			Deleting the inner pointer
			*/
            void release() throw()
            {
                delete this->m_raw_ptr;
            }

			/**
			Checks whether the inner pointer is not null
			*/
			bool is_valid() const throw()
			{
				return (this->m_raw_ptr != 0);
			}

        private:

			// disable default constructor
            unique_ptr();

			// disable copy constructor
            unique_ptr(const unique_ptr& r);

			// disable operators
			unique_ptr& operator=(const unique_ptr<T>& rhs);

			// disable alloc and dealloc on the heap
			void * operator new(size_t);
			void * operator new[](size_t);
			void operator delete(void *);
			void operator delete[](void*);

			// inner raw pointer
            T * m_raw_ptr;
	};
}

#endif
